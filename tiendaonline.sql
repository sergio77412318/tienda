-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 04:28 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiendaonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` int(2) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`) VALUES
(8, 'Barbijos'),
(9, 'Trajes Bioseguridad'),
(10, 'Mascaras');

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(3) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `cp` varchar(20) NOT NULL,
  `provincia` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `validado` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre`, `apellidos`, `email`, `direccion`, `cp`, `provincia`, `telefono`, `password`, `validado`) VALUES
(47, 'Sergio', 'Fernandez', 'sergioandia11@gmail.com', 'avenida fuerza aerea numero 2343, Avenida Fuerza A', '00000', 'Cochabamba', '77412318', '1234567890', 1);

-- --------------------------------------------------------

--
-- Table structure for table `codigos`
--

CREATE TABLE `codigos` (
  `id_codigo` int(3) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `fecha_antigua` int(15) NOT NULL,
  `id_cliente` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `codigos`
--

INSERT INTO `codigos` (`id_codigo`, `codigo`, `fecha_antigua`, `id_cliente`) VALUES
(22, '7lhqdJBSTl', 1592944670, 39),
(23, 'jefo5jFEPL', 1592944726, 40),
(24, 'daLJVwjUBV', 1592944813, 41),
(25, 'bTALdzl0Zw', 1592964102, 42),
(26, 'soa2sgRchG', 1593028895, 43),
(27, 'T9iJOKphVH', 1593053725, 44),
(28, 'zHZeN3PEv2', 1593111799, 45),
(29, 'aSav2gy25R', 1595098984, 46),
(30, 'S2sXpPfjEI', 1595113861, 47);

-- --------------------------------------------------------

--
-- Table structure for table `comentarios`
--

CREATE TABLE `comentarios` (
  `id_comentario` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `comentario` text COLLATE latin1_spanish_ci NOT NULL,
  `puntuacion` tinyint(1) NOT NULL,
  `correo` bit(1) NOT NULL,
  `url` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `imagenes`
--

CREATE TABLE `imagenes` (
  `id_imagen` int(4) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `prioridad` int(1) NOT NULL,
  `id_producto` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagenes`
--

INSERT INTO `imagenes` (`id_imagen`, `nombre`, `prioridad`, `id_producto`) VALUES
(106, '1593027674_01.jpg', 1, 74),
(107, '1593027759_01.jpg', 1, 75),
(108, '1593027759_02.jpg', 2, 75),
(109, '1593027787_01.jpg', 1, 76),
(110, '1593027828_01.jpg', 1, 77),
(111, '1593027874_01.jpg', 1, 78),
(112, '1593027874_02.jpg', 2, 78),
(113, '1593027874_03.jpg', 3, 78),
(114, '1593028382_01.jpg', 1, 79),
(115, '1593028382_02.jpg', 2, 79),
(116, '1593028416_01.jpg', 1, 80),
(117, '1593028440_01.jpg', 1, 81),
(118, '1593028464_01.jpg', 1, 82),
(119, '1593028634_01.jpg', 1, 83),
(120, '1593028667_01.jpg', 1, 84),
(121, '1593028701_01.jpg', 1, 85),
(122, '1593028701_02.jpg', 2, 85),
(123, '1593028733_01.jpg', 1, 86),
(124, '1593028792_01.jpg', 1, 87),
(125, '1593112079_01.jpg', 3, 88),
(126, '1593112079_02.jpg', 1, 88),
(127, '1593112079_03.jpg', 2, 88);

-- --------------------------------------------------------

--
-- Table structure for table `interruptor`
--

CREATE TABLE `interruptor` (
  `id_interruptor` int(11) NOT NULL,
  `estado` bit(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interruptor`
--

INSERT INTO `interruptor` (`id_interruptor`, `estado`) VALUES
(1, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `interruptor_stock`
--

CREATE TABLE `interruptor_stock` (
  `id` tinyint(1) NOT NULL,
  `estado` bit(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interruptor_stock`
--

INSERT INTO `interruptor_stock` (`id`, `estado`) VALUES
(1, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `pedidos`
--

CREATE TABLE `pedidos` (
  `pedido` int(5) NOT NULL,
  `producto` varchar(100) NOT NULL,
  `cantidad` int(2) NOT NULL,
  `precio_producto` double NOT NULL,
  `id_cliente` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pedidos`
--

INSERT INTO `pedidos` (`pedido`, `producto`, `cantidad`, `precio_producto`, `id_cliente`) VALUES
(4, 'BenQ XL2730Z 27 LED 144Hz QHD', 1, 599, 41),
(5, 'Barbijo', 3, 3, 45),
(6, 'marco', 1, 500, 45);

-- --------------------------------------------------------

--
-- Table structure for table `pedidos2`
--

CREATE TABLE `pedidos2` (
  `fecha_pedido` timestamp NOT NULL DEFAULT current_timestamp(),
  `total_pedido` double NOT NULL,
  `envio` bit(1) NOT NULL,
  `pago` varchar(50) NOT NULL,
  `estado` int(1) NOT NULL DEFAULT 0,
  `pedido` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pedidos2`
--

INSERT INTO `pedidos2` (`fecha_pedido`, `total_pedido`, `envio`, `pago`, `estado`, `pedido`) VALUES
('2020-06-25 19:05:14', 9, b'0', 'Efectivo', 2, 5),
('2020-06-25 19:08:51', 502.48, b'1', 'Contrareembolso', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(3) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `precio` double NOT NULL,
  `descripcion` text NOT NULL,
  `id_categoria` int(2) NOT NULL,
  `inicio` int(1) NOT NULL DEFAULT 0,
  `cantidad` smallint(6) NOT NULL DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre`, `precio`, `descripcion`, `id_categoria`, `inicio`, `cantidad`) VALUES
(74, 'Barbijo', 3, 'El producto contiene: 2 m?scaras (algod?n 100%) 8 chips de filtro (5 capas de protecci?n efectiva).\r\nEl dise?o de 2 m?scaras faciales es superior, el tejido de alta calidad es ligero y suave, se puede llevar en cualquier momento. Esta m?scara protege contra el polvo, las secreciones y los contaminantes.\r\nLas 3 m?scaras est?n hechas de algod?n de alta calidad para que sean suaves y lavables, reutilizables y se pueden plegar para un f?cil transporte.\r\n4 amplia aplicaci?n: esta m?scara antipolvo es ideal tanto para mujeres como para hombres, adecuado para ciclismo, camping, correr, viajes, escalada y uso diario. Te protege de la niebla, la niebla, el escape del veh?culo, el tabaquismo pasivo, etc.\r\n5. Precauci?n: la m?scara se puede lavar y reutilizar, y el filtro no es lavable. Es un art?culo desechable (el chip del filtro es seguro durante 7-10 d?as).', 8, 1, 6),
(75, 'Barbijos', 5, '100% Cotton\r\nAn Amazon Brand\r\nFeaturing a double layer of lightweight jersey fabric for breathability and comfort, these face coverings were designed for a snug but comfortable fit\r\nThe CDC recommends the use of cloth face coverings while in public to reduce community spread by asymptomatic persons\r\nFace coverings are not FDA-approved, not intended for medical use, and not proven to reduce the transmission of disease.\r\nPlease reference the provided size chart on the left to ensure a perfect fit\r\nMachine wash before first use and after every use\r\nMade in Guatemala', 8, 1, 5),
(76, 'Barbijo1', 4, '100% Cotton\r\nAn Amazon Brand\r\nFeaturing a double layer of lightweight jersey fabric for breathability and comfort, these face coverings were designed for a snug but comfortable fit\r\nThe CDC recommends the use of cloth face coverings while in public to reduce community spread by asymptomatic persons\r\nFace coverings are not FDA-approved, not intended for medical use, and not proven to reduce the transmission of disease.\r\nPlease reference the provided size chart on the left to ensure a perfect fit\r\nMachine wash before first use and after every use\r\nMade in Guatemala', 8, 1, 18),
(77, 'Barbijo2', 6, '100% Cotton\r\nAn Amazon Brand\r\nFeaturing a double layer of lightweight jersey fabric for breathability and comfort, these face coverings were designed for a snug but comfortable fit\r\nThe CDC recommends the use of cloth face coverings while in public to reduce community spread by asymptomatic persons\r\nFace coverings are not FDA-approved, not intended for medical use, and not proven to reduce the transmission of disease.\r\nPlease reference the provided size chart on the left to ensure a perfect fit\r\nMachine wash before first use and after every use\r\nMade in Guatemala', 8, 1, 20),
(78, 'Barbijo3', 6, '100% Cotton\r\nAn Amazon Brand\r\nFeaturing a double layer of lightweight jersey fabric for breathability and comfort, these face coverings were designed for a snug but comfortable fit\r\nThe CDC recommends the use of cloth face coverings while in public to reduce community spread by asymptomatic persons\r\nFace coverings are not FDA-approved, not intended for medical use, and not proven to reduce the transmission of disease.\r\nPlease reference the provided size chart on the left to ensure a perfect fit\r\nMachine wash before first use and after every use\r\nMade in Guatemala', 8, 0, 60),
(79, 'Traje', 100, 'Ligero, duradero rip-stop frente de nailon\r\nRespaldo de algod?n transpirable y fresco con su?ter completo.\r\nCierre de velcro en la mu?eca y el tobillo.\r\nCremallera frontal y cintura el?stica.\r\nLavable y reutilizable.', 9, 1, 5),
(80, 'Traje1', 70, 'Ligero, duradero rip-stop frente de nailon\r\nRespaldo de algod?n transpirable y fresco con su?ter completo.\r\nCierre de velcro en la mu?eca y el tobillo.\r\nCremallera frontal y cintura el?stica.\r\nLavable y reutilizable.', 9, 1, 9),
(81, 'Traje2', 90, 'Ligero, duradero rip-stop frente de nailon\r\nRespaldo de algod?n transpirable y fresco con su?ter completo.\r\nCierre de velcro en la mu?eca y el tobillo.\r\nCremallera frontal y cintura el?stica.\r\nLavable y reutilizable.', 9, 0, 20),
(82, 'Traje3', 190, 'Ligero, duradero rip-stop frente de nailon\r\nRespaldo de algod?n transpirable y fresco con su?ter completo.\r\nCierre de velcro en la mu?eca y el tobillo.\r\nCremallera frontal y cintura el?stica.\r\nLavable y reutilizable.', 9, 0, 25),
(83, 'Mascara', 18, 'Caracter?sticas: resistente al viento, al polvo, antigoteo, protector facial.\r\nProtecci?n total contra ca?das, polvo, humo de aceite, etc.\r\nEl?stico ajustable y adecuado tanto para ni?os como para adultos.\r\nNota: Retira la pel?cula antiara?azos antes de usar.\r\nLa visera se puede lavar y reutilizar.', 10, 0, 20),
(84, 'Mascara1', 18, 'Caracter?sticas: resistente al viento, al polvo, antigoteo, protector facial.\r\nProtecci?n total contra ca?das, polvo, humo de aceite, etc.\r\nEl?stico ajustable y adecuado tanto para ni?os como para adultos.\r\nNota: Retira la pel?cula antiara?azos antes de usar.\r\nLa visera se puede lavar y reutilizar.', 10, 1, 6),
(85, 'Mascara2', 90, 'Caracter?sticas: resistente al viento, al polvo, antigoteo, protector facial.\r\nProtecci?n total contra ca?das, polvo, humo de aceite, etc.\r\nEl?stico ajustable y adecuado tanto para ni?os como para adultos.\r\nNota: Retira la pel?cula antiara?azos antes de usar.\r\nLa visera se puede lavar y reutilizar.', 10, 0, 8),
(86, 'Mascara3', 8, 'Caracter?sticas: resistente al viento, al polvo, antigoteo, protector facial.\r\nProtecci?n total contra ca?das, polvo, humo de aceite, etc.\r\nEl?stico ajustable y adecuado tanto para ni?os como para adultos.\r\nNota: Retira la pel?cula antiara?azos antes de usar.\r\nLa visera se puede lavar y reutilizar.', 10, 0, 100),
(87, 'Mascara premium', 100, 'Caracter?sticas: resistente al viento, al polvo, antigoteo, protector facial.\r\nProtecci?n total contra ca?das, polvo, humo de aceite, etc.\r\nEl?stico ajustable y adecuado tanto para ni?os como para adultos.\r\nNota: Retira la pel?cula antiara?azos antes de usar.\r\nLa visera se puede lavar y reutilizar.', 10, 1, 4),
(88, 'marco', 500, 'Marco aguilar st', 9, 0, 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `codigos`
--
ALTER TABLE `codigos`
  ADD PRIMARY KEY (`id_codigo`);

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id_comentario`);

--
-- Indexes for table `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_imagen`);

--
-- Indexes for table `interruptor`
--
ALTER TABLE `interruptor`
  ADD PRIMARY KEY (`id_interruptor`);

--
-- Indexes for table `interruptor_stock`
--
ALTER TABLE `interruptor_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `codigos`
--
ALTER TABLE `codigos`
  MODIFY `id_codigo` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id_imagen` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `interruptor`
--
ALTER TABLE `interruptor`
  MODIFY `id_interruptor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `interruptor_stock`
--
ALTER TABLE `interruptor_stock`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
